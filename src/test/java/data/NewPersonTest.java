package data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NewPersonTest {

    @Test
    void testCompareToEquals() {
        var NewPersonA = new NewPerson();
        NewPersonA.name = "A";
        var NewPersonB = new NewPerson();
        NewPersonB.name = "A";

        assertEquals(0, NewPersonA.compareTo(NewPersonB));
    }

    @Test
    void testCompareToAB() {
        //A before B
        var NewPersonA = new NewPerson();
        NewPersonA.name = "A";
        var NewPersonB = new NewPerson();
        NewPersonB.name = "B";

        assertEquals(-1, NewPersonA.compareTo(NewPersonB));
    }

    @Test
    void testCompareToBA() {
        //B before A
        var NewPersonA = new NewPerson();
        NewPersonA.name = "B";
        var NewPersonB = new NewPerson();
        NewPersonB.name = "A";

        assertEquals(1, NewPersonA.compareTo(NewPersonB));
    }
}
