package data;

public class NewPerson implements Comparable<NewPerson>{
    public String name;
    public String company;
    public String date;
    public String color;
    
    @Override
    public int compareTo(NewPerson otherPerson) {
        return this.getName().compareTo(otherPerson.getName());
    }

    String getName() {
        return this.name;
    }

    String getColor(){
        return this.color;
    }

    String getDate(){
        return this.date;
    }

    String getCompany(){
        return this.company;
    }
}

