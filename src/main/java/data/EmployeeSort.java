package data;
import java.io.*;
import java.util.*;
import java.util.Collections;


public class EmployeeSort {
    
    public static void main (String args[]) throws IOException{
        File newFile = new File ("src/main/java/data/project-data.txt");
        parse(newFile);
        //System.out.println("test");
    } 


    public static List<NewPerson> parse(File data) throws IOException{

        BufferedReader reader = new BufferedReader(new FileReader(data));

        List<NewPerson> employees = new ArrayList<>();

        // for reading one line
        String line = null;

        NewPerson temp = new NewPerson();

        try {
            
            while ((line = reader.readLine()) != null) {

                
                String[] split = line.split(":");
                //split at : for txt file

                switch (split[0]) {
                    case "Date":
                        temp.date = split[1];
                        break;
                    case "Name":
                        temp.name = split[1];
                        break;
                    case "Company":
                        temp.company = split[1];
                        break;
                    case "Color":
                        //since color is last element listed per person, add temp person in this case
                        temp.color = split[1];
                        employees.add(temp);
                    
                        temp = new NewPerson();
                        break;
                    default:
                        break;
                }


            }
        } catch(Exception e) {
            System.out.println(e);
        }

        reader.close();

        Collections.sort(employees);
        //sort in alphabetical order using compareTo in NewPerson.java

        for (int i = 0; i < employees.size();i++) { 		      
            System.out.println(employees.get(i).getName()+", " +employees.get(i).getColor()+", "+employees.get(i).getDate()+", "+ employees.get(i).getCompany()); 		
        }   
        
        return employees;
        //return array list in alphabetical order via first name
        
  } //end of parse


  public static List<NewPerson> parse(String string) {
    try {
        return parse(new File (string));
    } catch (IOException e) {
        //TODO Auto-generated catch block
        e.printStackTrace();
        return Collections.emptyList();
    }
}

}
